#include <iostream>
#include <string>
#include "Osoba.h"

using namespace std;
 
int main() {
    cout << "Ilość osób = " << Osoba::getIle() << endl;

    cout << "Tworzymy mężczyznę" << endl;
    Osoba* pierwszyMezczyzna = new Osoba("Adam", "Nowak", 1975);
    cout << "Ilość osób = " << Osoba::getIle() << endl;
    pierwszyMezczyzna->przedstawSie();

    cout << "Tworzymy kobietę" << endl;
    Osoba* pierwszaKobieta = new Osoba("Ewa", "Nowak", 1982);
    cout << "Ilość osób = " << Osoba::getIle() << endl;
    pierwszaKobieta->przedstawSie();

    cout << "Tworzymy dziecko" << endl;
    Osoba* dziecko = new Osoba("Piotr");
    cout << "Ilość osób = " << Osoba::getIle() << endl;
    dziecko->przedstawSie();
	
	cout << endl;
	cout << "Tworzymy rodzinę" << endl;
		
	Osoba* rodzina[3];
	
	for (int i=0;i<3;i++) {
	rodzina[i] = new Osoba;
	}
	
	rodzina[0] = pierwszyMezczyzna;
	rodzina[1] = pierwszaKobieta;
	rodzina[2] = dziecko;
	
	for (int i=0;i<3;i++) {
	rodzina[i]->przedstawSie();
	}

	
    cout << "Usuwamy mężczyznę" << endl;
    delete pierwszyMezczyzna;
    cout << "Ilość osób = " << Osoba::getIle() << endl;

}

