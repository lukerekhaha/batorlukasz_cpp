#include <iostream>
#include "Osoba.h"

using namespace std;

int Osoba::ile = 0;

Osoba::Osoba(const string& imie,const string& nazwisko, int rokUrodzenia)
    : imie(imie), nazwisko(nazwisko), rokUrodzenia(rokUrodzenia)
{
    ++ile;
}
 
Osoba::Osoba(const string& imie)
    : imie(imie)
{
    ++ile;
}

Osoba::Osoba() {
}

Osoba::~Osoba()
{
    --ile;
}



void Osoba::przedstawSie() const {
    cout << "Mam na imię " + imie << " " << nazwisko << " i urodził(e/a)m się w" << endl; 
    cout << rokUrodzenia << " roku." << endl;
}

int Osoba::getIle() {
    return ile;
}
