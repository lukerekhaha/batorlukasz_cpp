#ifndef NAZWANYPUNKT_H
#define NAZWANYPUNKT_H
#include "Punkt.h"
#include <string>

class NazwanyPunkt: public Punkt{
	public:
	NazwanyPunkt(int,int,std::string);
	 
	
	private:
	std::string nazwa;
	
};




#endif
