#include <iostream>
#include "Prostokat.h"
#include <string>
#include "Figura.h"
using namespace std;

Prostokat::Prostokat(string const& kolor, float x, float y): kolor(kolor), x(x), y(y) {}

float Prostokat::getPole() const 
{
	return x*y;
}
  
