#ifndef FIGURA_H
#define FIGURA_H

class Figura {
public:
    Figura(std::string const& );
    virtual ~Figura()= default;
    std::string getKolor() const; 
    virtual float getPole() const;
    
protected:
	std::string kolor;
};

#endif
 
 
