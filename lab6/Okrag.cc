#include <iostream>
#include "Okrag.h"
#include <string>
#include "Figura.h"

using namespace std;

Okrag::Okrag(string const& kolor, float r): kolor(kolor), r(r) {}

float Okrag::getPole() const 
{
	return 3.14*r*r;
}
 
