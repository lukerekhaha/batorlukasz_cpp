#ifndef TABLICA_H
#define TABLICA_H

class Tablica;
{

	
private:
	int length;
	long* tab;

	public:
	Tablica(int len, long val = 0);
	Tablica(const Tablica&); // konstruktor kopiuj�cy 
	~Tablica();
	Tablica& operator=(const Tablica&); //operator przypisania 
	
	void setElement(int j, long v);
	long getElement(int j) const;
	int getLength() const;
	
};

 
#endif
