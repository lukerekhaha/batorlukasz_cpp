#include <iostream>
#include "Tablica.h"

using namespace std;

void pokaz(const Tablica& t)
{
	int l = t.getLength();
	cout << "Length: " << l << endl;
	for(int i = 0; i < l; i++)
		cout << i << ". " << t.getElement(i) << "\t";
		
	cout << endl;
}

int main()
{
	Tablica a(12);
	pokaz(a);
	Tablica b(a);
	pokaz(b);
	for(int i = 0; i < a.getLength(); i++) a.setElement(i, 100-i);
	pokaz(a);
	b = a;
	pokaz(b);
}
 
