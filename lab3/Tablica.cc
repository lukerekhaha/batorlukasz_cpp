#include "Tablica.h"

Tablica::Tablica(int len, long val)
	:length(len)
{
	if(len > 0)
	{
		tab = new long[len];
		for(int i = 0; i < len; i++) tab[i] = val;
	}
}

Tablica::Tablica(const Tablica& t)
{
	if(t.tab != nullptr)
	{
		length = t.length;
		tab = new long[length];
		for(int i = 0; i < length; i++) tab[i] = t.tab[i];
	}
}
 
Tablica::~Tablica()
{
	delete [] tab;
}

Tablica& Tablica::operator=(const Tablica& t)
{
	if(this == &t) return *this;
	if(tab != nullptr) delete [] tab;
	length = t.length;
	tab = nullptr;
	if(length)
	{
		tab = new long[length];
		for(int i = 0; i < length; i++) tab[i] = t.tab[i];
	}
	return *this;
}

void Tablica::setElement(int j, long v)
{
	if(j < length) tab[j] = v;
}

long Tablica::getElement(int j) const
{
	return tab[j];
}

int Tablica::getLength() const
{
	return length;
}
