
#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

class Licznik 
{
	public:
		void zwieksz() { wartosc++; };
		void zmniejsz() { wartosc--; };
		int getWartosc() { return wartosc; };
		
	private:
		int wartosc = 0; 
};

int main() 
{
	Licznik licznik;
	licznik.zwieksz(); 
	licznik.zwieksz();
	licznik.zwieksz();
	printf("%d\n", licznik.getWartosc());
	licznik.zmniejsz();
	printf("%d", licznik.getWartosc());	

} 


